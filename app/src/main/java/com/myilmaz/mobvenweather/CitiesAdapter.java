package com.myilmaz.mobvenweather;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    private final String[] mData;
    private ViewHolder myOldCityHolder;

    public CitiesAdapter(String[] cities) {
        mData = cities;
    }

    @Override
    public CitiesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // create city and set name
        final String city = mData[position];
        holder.tvCity.setText(city);

        // set background color
        holder.itemView.setBackgroundResource(position % 2 == 1 ? R.color.gray_light : R.color.white);

        // show done icon if it is my city
        if (city.equals(SharedPrefManager.getMyCity())) {
            myOldCityHolder = holder;
            holder.ivDone.setVisibility(View.VISIBLE);
        } else {
            holder.ivDone.setVisibility(View.INVISIBLE);
        }

        // change my city
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefManager.setMyCity(city);
                if (myOldCityHolder != null) {
                    myOldCityHolder.ivDone.setVisibility(View.INVISIBLE);
                }
                myOldCityHolder = holder;
                holder.ivDone.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_item_city_name)
        TextView tvCity;
        @BindView(R.id.iv_item_city_done)
        ImageView ivDone;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

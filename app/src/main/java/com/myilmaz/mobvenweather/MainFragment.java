package com.myilmaz.mobvenweather;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */

public class MainFragment extends Fragment {

    public static final String OPEN_WEATHER_MAP_ICON_BASE_URL = "http://openweathermap.org/img/w/";

    @BindView(R.id.iv_main_weather_icon)
    ImageView mIvWeatherIcon;
    @BindView(R.id.tv_main_weather_title)
    TextView mTvTitle;
    @BindView(R.id.tv_main_weather_desc)
    TextView mTvDesc;
    @BindView(R.id.tv_main_temp_min)
    TextView mTvTempMin;
    @BindView(R.id.tv_main_temp_max)
    TextView mTvTempMax;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);

        final String city = SharedPrefManager.getMyCity();

        MainApplication.getWeatherService().forecast(city + ",tr", 1).enqueue(new Callback<Forecast>() {
            @Override
            public void onResponse(Call<Forecast> call, Response<Forecast> response) {
                Forecast forecast = response.body();
                Glide.with(getContext()).load(OPEN_WEATHER_MAP_ICON_BASE_URL + forecast.getIcon() + ".png").into(mIvWeatherIcon);
                int tempMin = Math.round(forecast.getWeather().temp_min);
                int tempMax = Math.round(forecast.getWeather().temp_max);
                mTvTitle.setText(getString(R.string.weather_title_format, tempMin, tempMax));
                mTvDesc.setText(getString(R.string.weather_desc_format, city, forecast.getWeatherStatus()));
                mTvTempMin.setText(getString(R.string.temperature_format, tempMin));
                mTvTempMax.setText(getString(R.string.temperature_format, tempMax));
            }

            @Override
            public void onFailure(Call<Forecast> call, Throwable t) {
                Snackbar.make(rootView, "Hava durumu bilgisi alınamadı!", Snackbar.LENGTH_LONG).show();
            }
        });

        return rootView;
    }
}

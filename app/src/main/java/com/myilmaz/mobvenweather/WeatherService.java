package com.myilmaz.mobvenweather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */
public interface WeatherService {

    @GET("forecast")
    Call<Forecast> forecast(@Query("q") String city, @Query("cnt") int dayCount);
}
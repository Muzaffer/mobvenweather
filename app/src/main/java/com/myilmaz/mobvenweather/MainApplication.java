package com.myilmaz.mobvenweather;

import android.app.Application;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tumblr.remember.Remember;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */

public class MainApplication extends Application {

    private static final String OPEN_WEATHER_MAP_URL = "http://api.openweathermap.org/data/2.5/";
    private static final String OPEN_WEATHER_MAP_APP_ID = "0dad9c5e053a4d713454484891490415";

    private static WeatherService mWeatherService;

    @Override
    public void onCreate() {
        super.onCreate();
        Remember.init(this, getPackageName());
    }

    public static WeatherService getWeatherService() {
        if (mWeatherService == null) {

            Interceptor queryInterceptor = new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    HttpUrl url = request.url().newBuilder()
                            .addQueryParameter("appid", OPEN_WEATHER_MAP_APP_ID)
                            .addQueryParameter("units", "metric").build();
                    request = request.newBuilder().url(url).build();
                    return chain.proceed(request);
                }
            };
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(queryInterceptor).build();

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            Retrofit retrofit = new Retrofit.Builder().baseUrl(OPEN_WEATHER_MAP_URL)
                    .client(client).addConverterFactory(JacksonConverterFactory.create(mapper)).build();
            mWeatherService = retrofit.create(WeatherService.class);
        }
        return mWeatherService;
    }
}

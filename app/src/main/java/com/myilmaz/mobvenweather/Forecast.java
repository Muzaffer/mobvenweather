package com.myilmaz.mobvenweather;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */

public class Forecast {

    public WeatherObject[] list;

    public static class WeatherObject {
        public Main main;
        public Weather[] weather;
    }

    public static class Main {
        public float temp;
        public float temp_min;
        public float temp_max;
    }

    public static class Weather {
        public String main;
        public String description;
        public String icon;
    }

    public String getWeatherStatus() {
        return list[0].weather[0].description;
    }

    public Main getWeather() {
        return list[0].main;
    }

    public String getIcon() {
        return list[0].weather[0].icon;
    }
}

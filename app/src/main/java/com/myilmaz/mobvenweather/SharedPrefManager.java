package com.myilmaz.mobvenweather;

import com.tumblr.remember.Remember;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */
public class SharedPrefManager {

    private static final String MY_CITY = "MY_CITY";

    public static void setMyCity(String city) {
        Remember.putString(MY_CITY, city);
    }

    public static String getMyCity() {
        return Remember.getString(MY_CITY, "İzmir");
    }
}

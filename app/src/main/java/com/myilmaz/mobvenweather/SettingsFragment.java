package com.myilmaz.mobvenweather;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muzaffer YILMAZ on 22.1.2018.
 */

public class SettingsFragment extends Fragment {

    @BindView(R.id.rv_settings_cities)
    RecyclerView mRvCities;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, rootView);

        mRvCities.setHasFixedSize(true);
        mRvCities.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvCities.setAdapter(new CitiesAdapter(getResources().getStringArray(R.array.cities)));

        return rootView;
    }
}
